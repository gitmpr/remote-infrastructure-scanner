#!/usr/local/bin/python3

import sys
import dns
import dns.resolver
import colorama
from colorama import init, deinit, Fore, Style, Back
import argparse
import subprocess
import subprocess_tee
import pathlib
from pathlib import Path
from url_parser import parse_url
import requests
from requests.exceptions import Timeout
import json
from pygments import highlight, lexers, formatters
import time
import polling2
import polling
import logging
from datetime import datetime
import ipaddress
import os
import getpass

# for debugging
import pprint
pp = pprint.pprint

# Phase 1: write correct CLI commands
# Phase 2: execute tools in project folder structure
# Phase 3: interpret results and write findings to output
# Phase 4: add findings to BB. Probably import from BB.

# parse arguments
parser = argparse.ArgumentParser(
    description = 'Remote Infrastructure scanning script using Dig, Nmap, cURL, Nikto, TestSSL, Nessus',
    epilog = 'Only valid IPv4 adresses and/or URLs with valid TLDs can be used as input'
    )


##parser.add_argument("inputdomains", nargs='*', help="one or more domains specified without protocol, separated by spaces")
parser.add_argument("targets", nargs='*', help="one or more domains and/or IPv4 addresses, separated by spaces")

parser.add_argument("-o", "--optional", help="show optional commands", action="store_true")
parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")

## removed this switch to make it harder to accidentally run this on non testing environment, possibly causing downtime.
## now only tells you the command to run it manually. you'd have to copy it from the script output and paste it in your terminal
# parser.add_argument("--slowhttptest", help="run the SlowHttpTest script", action="store_true")

## add argument to specify file with domain names to scan
parser.add_argument("-d", "--targetslistfile", help="specify optional list file with targets, separated by newlines", action="store")


# arguments to enable scans that do NOT run by default:

## add argument to run nmap commands
parser.add_argument("--nmap", help="run the nmap commands", action="store_true")



## add argument to run nessus commands
parser.add_argument("--nessus", help="run the nessus commands", action="store_true")
## add argument to specify nessus outputname
parser.add_argument("-O", "--nessus_outputname", default="", help="specify optional outputname for nessus scan reports", action="store")
## add arguments to specify Nessus login credentials
parser.add_argument("-u", "--nessus_username", default="", help="specify nessus login username, default = administrator", action="store")
parser.add_argument("-p", "--nessus_password", default="", help="specify nessus login password", action="store")
## add argument to specify everything after running script, answering interactive questions TODO TODO
parser.add_argument("-w", "--wizard", default=False, help="start the script as a wizard, asking interactive questions on command line", action="store")


# arguments to disable scans that do run by default:

## add argument to NOT run dig command. does run by default
parser.add_argument("--nodig", help="do not run the dig command", action="store_true")

## add argument to NOT run testssl command. does run by default
parser.add_argument("--notestssl", help="do not run the testssl script", action="store_true")

## add argument to NOT run nikto command. does run by default
parser.add_argument("--nonikto", help="do not run the nikto script", action="store_true")

## add argument to NOT run curl command. does run by default
parser.add_argument("--nocurl", help="do not run the curl command", action="store_true")




args = parser.parse_args()


#colorama
init()

## 



##input precedence overrides
##input options answered if interactive wizard is invoked with -w option, fill in default options with command line parameters and/or env vars TODO
##input options/flags/arguments/parameters appended on command line
##input from textfile, also specified as command line parameter
##input options set in environment variables



# export RIS_ENV_TEST_INT=15 && export RIS_ENV_TEST_STR=abc
# ./export_env_vars.sh

## TODO env vars
### print(os.environ.get('RIS_ENV_TEST_INT'))
### print(os.environ.get('RIS_ENV_TEST_STR'))

## helper functions
# pretty print json
def ppjs(dict_or_string):
    if (type(dict_or_string) == dict):
        # print("is dict")
        json_dict=dict_or_string
    elif (type(dict_or_string) == str):
        # print("is string")
        json_string=dict_or_string
        json_dict = json.loads(json_string)
    else:
        raise ValueError('invalid argument type for prettyprint json function')
    jdump=json.dumps(json_dict, indent=2) # convert to indented json string
    colorful_json = highlight(jdump, lexers.JsonLexer(), formatters.Terminal256Formatter(style='monokai'))
    print(colorful_json) 

# print response status and optionally reponse body of http request
def request_log(request, printresponse=True, appendnewline=True):
    print(Fore.YELLOW + request.url +  Style.RESET_ALL)
    print('HTTP status code ' + str(request.status_code))
    if request:
        print('request successful')     
    else:
        print('An error has occured in the HTTP ' + request.request.method + ' request')     
    if printresponse:
        if (request.headers['Content-Type'] == 'text/json'):
            ppjs(request.text)
        elif (request.headers['Content-Type'] == 'application/json'):
            ppjs(request.text)
        else:
            print(request.text)        
    if appendnewline: print()



# function to filter out duplicate entries from list
def unique(list):
    unique_list = []
    for item in list:
        if item not in unique_list:
            unique_list.append(item)
    return unique_list
# while converting to set and then back to list is faster, this implementation keeps list order with older python versions in which sets don't preserve order.




targets = args.targets ## list of positional first arguments: domain names and/or IPv4 addresses



# read targets from file
if args.targetslistfile:
    with open(args.targetslistfile, 'r') as f:
        lines = f.readlines()
    non_empty_lines = [line.strip() for line in lines if line.strip() != ""] ## remove empty lines, and leading and trailing whitespace
    targets += non_empty_lines ## append targets from input list file to targets from command line input
    

if targets == []:
    print('Specify input domains or IPv4 addresses')
    quit()


targets_unique = unique(targets)
if targets_unique != targets:
    print('Warning: duplicate target entries detected, removing duplicates')
    targets = targets_unique


domains = []
ips = []

for target in targets:
    try:
        domains.append( {
                            'input_domain'  : target,
                            'parsed_domain' : parse_url(target)  ## fails if not a valid domain name
                        }
        )
        
        ## put input_domain and parsed_domain in same dictionary to avoid excessive indexing
#        parsed_domain = parse_url(target)
#        domains.append( {'input_domain': target} | parsed_url )
        
    except:
        try:
            ips.append( {
                            'input_ip'  : target,
                            'parsed_ip' : ipaddress.IPv4Address(target)  ## fails if not a valid IPv4
                        } 
            ) 
        except:
            print(F'unparsable input entry: {target}, aborting') ## if both fail, abort the script
            print('only domains with valid TLDs, and IPv4 host addresses can be parsed')
            quit()

for domain in domains:
    print('inputdomain: ' + domain["input_domain"])

for ip in ips:
    print('inputip:     ' + ip["input_ip"])


print()









for domain in domains:
    
    # don't include the period "." character in sub_domain part if sub_domain is "www" (weird behavior of url_parser library)
    if domain['parsed_domain']['sub_domain'] == 'www.':
        domain['parsed_domain']['sub_domain'] = 'www'
    
    ## TODO: lets assume everything is https for now, and ignore protocol, port, path, query string, and fragment here
    
    
    ## example case:
    ## pwnadventure.acme.cc OR acme.cc
    if domain['parsed_domain']['sub_domain'] != None:
        domain['url'] = (
            domain['parsed_domain']['sub_domain'] + \
            '.' + \
            domain['parsed_domain']['domain'] + \
            '.' + \
            domain['parsed_domain']['top_domain']
        )
    else:
        domain['url'] = (
            domain['parsed_domain']['domain'] + \
            '.' + \
            domain['parsed_domain']['top_domain']
        )
    
    ## acme.cc
    domain['url_without_sub_domain'] = (
        domain['parsed_domain']['domain'] + '.' + \
        domain['parsed_domain']['top_domain']
    )
    
    ## pwnadventure-acme-cc
    domain['url_outputname'] = domain['url'].replace('.', '-')
    
    ## acme-cc
    domain['url_without_sub_domain_outputname'] = domain['url_without_sub_domain'].replace('.', '-')
    
    print('The URL is: ' + domain['url'] )
    
    if domain['parsed_domain']['sub_domain'] != None:
        print(F'The sub_domain is: {domain["parsed_domain"]["sub_domain"]}')
    else:
        print('No sub_domain specified')



    # Determine the IPv4 and IPv6 addresses of the host
    
    domain['ipv4list'] = []
    try:
	    domain['ipv4list'] = dns.resolver.resolve(domain['url'], 'A') ## returns a list if multiple IP addresses are behind one domain name. 
	    for ipval in domain['ipv4list']:
		    print('IPv4 address is: ' + Fore.BLUE + ipval.to_text() + Style.RESET_ALL)
		    #domain['ipv4list'].append(ipval.to_text())
		
    except:
	    print('IPv4 address ' + Fore.RED + 'not' + Style.RESET_ALL + ' detected.')

    domain['ipv6list'] = []
    try:
	    domain['ipv6list'] = dns.resolver.resolve(domain['url'], 'AAAA')
	    for ipval6 in domain['ipv6list']:
		    print('IPv6 address is: ' + Fore.BLUE + ipval6.to_text() + Style.RESET_ALL)
		    #domain['ipv6list'].append(ipval6.to_text())
    except:
	    print('IPv6 address ' + Fore.RED + 'not' + Style.RESET_ALL + ' detected.')

    print()


for ip in ips:
    ip['outputname'] = ip['input_ip'].replace('.', '-')
    


 
def is_part_of_network(ipaddress, ipnetwork):
    if ipaddress in ipnetwork:
        print(F'yes, the IP address {ipaddress} is part of the network {ipnetwork}')
        return True
    else:
        print(F'no, the IP address {ipaddress} is NOT part of the network {ipnetwork}') 
        return False

#cloudflareip4listresponse = requests.get("http://www.cloudflare.com/ips-v4", timeout=1)


# get cloudflare IPs to exclude from scans
try:
    cloudflareip4listresponse = requests.get("http://www.cloudflare.com/ips-v4", timeout=1)
#    cloudflareip4list = cloudflareip4listresponse

except Timeout:
    cloudflareip4list = \
     """173.245.48.0/20
        103.21.244.0/22
        103.22.200.0/22
        103.31.4.0/22
        141.101.64.0/18
        108.162.192.0/18
        190.93.240.0/20
        188.114.96.0/20
        197.234.240.0/22
        198.41.128.0/17
        162.158.0.0/15
        104.16.0.0/13
        104.24.0.0/14
        172.64.0.0/13
        131.0.72.0/22"""
    print('The cloudflare networks ip addresses list request timed out, using static list')
        
else:
    print('The cloudflare networks ip addresses request was successful, using response list')
    cloudflareip4list = cloudflareip4listresponse



cloudflareipaddressnetwork_string_list = cloudflareip4list.text.split("\n")
parsedcloudflareipnetworks = list(map(ipaddress.ip_network, cloudflareipaddressnetwork_string_list))
#parsedcloudflareipnetworks = [ipaddress.ip_network(ip) for ip in cloudflareipaddressnetwork_string_list]
#cloudflareipaddressnetwork_list = cloudflareip4list.text.split("\n")

def check_ip_is_in_cloudflare(ipaddress):
    returnvalue = False
    for cf_network in parsedcloudflareipnetworks:
        if ipaddress in cf_network:
            returnvalue = True
    if returnvalue:
        print(F'We have a Cloudflare IP address: {ipaddress.exploded}')
    else:
        print(F'IP address {ipaddress.exploded} is not part of any Cloudflare network')
    return returnvalue



for domain in domains:
    domain['ipv4list_nocloudflare'] = []
    for ipv4 in domain['ipv4list']:
        ipaddr = ipaddress.ip_address(ipv4.to_text() )
        if check_ip_is_in_cloudflare(ipaddr) == False:
            domain['ipv4list_nocloudflare'].append(ipv4)

ips_nocloudflare = []

for ip in ips:
    if check_ip_is_in_cloudflare(ip['parsed_ip']) == False:
        ips_nocloudflare.append(ip)

print()










if args.notestssl != True:
    dig_results = {}

    def dig(domain):
        results_dig_dict = {}
        for record in ['any', 'A', 'CAA', 'DNSSEC', 'NS']:
        
            if record == ('any' or 'A'  ): 
                dig_input = "url"           ## is full url, with subdomain if exists
            elif record == ('CAA' or 'DNSSEC' or 'NS'):
                dig_input = "url_without_sub_domain" ## secondleveldomain is url without subdomain
                
            if record == 'DNSSEC': 
                recordspec = '+dns +multi'
            else:
                recordspec = record

            ## TODO: subprocess_tee can be used instead of checking and printing the result separately
    #        results_dig_dict[record] = subprocess_tee.run(['dig', domain[dig_input], recordspec, '@8.8.8.8'], capture_output=True, text=True)
            results_dig_dict[record] = subprocess.run(['dig', domain[dig_input], recordspec, '@8.8.8.8'], capture_output=True, text=True)

            if results_dig_dict[record].returncode == 0:
                
                if not os.path.exists('DIG'):
                    os.makedirs('DIG')
                    
                if dig_input == "url":
                    f = open('DIG/' + domain['url_outputname'] + '-dig-' + record + '.txt', 'w')
                elif dig_input == "url_without_sub_domain":
                    f = open('DIG/' + domain['url_without_sub_domain_outputname'] + '-dig-' + record + '.txt', 'w')
                
    #            print(results_dig_dict[record])
                f.write(results_dig_dict[record].stdout)
                f.close()
    #-#            if args.verbose:
                print(results_dig_dict[record].stdout)
    #-#        else:
    #-#            print(results_dig_dict[record].stdout)


        # domain['dig'] = results_dig_dict
        dig_results[ domain['url'] ] = results_dig_dict


    for domain in domains:
        print(Fore.GREEN + 'Running dig on: ' + Fore.BLUE + domain['url'] + Style.RESET_ALL)
        dig(domain)




## TODO: check logic with default value not set right
## concat secondleveldomains
if args.nessus_outputname: 
    nessus_outputname = args.nessus_outputname
else:
    nessus_outputname = "nessus"
    for domain in domains:
        nessus_outputname += '_' + domain['url_without_sub_domain_outputname']
    for ip in ips_nocloudflare:
        nessus_outputname += '_' + ip['outputname'] 
        






nessustargets = []
## TODO: add ips from env vars
## TODO: add ips from wizard

## TODO: add domains from env vars
## TODO: add domains from wizard


#### Nessus input targets specification:
#### Example: 192.168.1.1-192.168.1.5, 192.168.2.0/24, test.com


# input domain names
for domain in domains:
    nessustargets.append(domain['url'])

# IPv4 addresses from input domains DNS lookups, minus the cloudflare ones
for domain in domains:
    for ipv4 in domain['ipv4list_nocloudflare']:
        nessustargets.append(ipv4.to_text() )

# input IPv4 addresses, minus the cloudflare ones
for ip in ips_nocloudflare:
    # check if one of the input ips wasn't also the DNS lookup result of one of the input domains, to prevent duplicate targets in nessus 
    if ip['parsed_ip'].exploded not in nessustargets: 
        nessustargets.append(ip['parsed_ip'].exploded)



#    
#nessustargets + ', '.join(ips)
print('nessustargets: ' + ', '.join(nessustargets) )
print()

# filter out duplicate ip addresses and domains
nessus_text_targets = ', '.join(unique(nessustargets))

print('unique nessustargets: ' + nessus_text_targets )
print()







if args.nessus:
    Path("Nessus").mkdir(parents=True, exist_ok=True)
    
    if (args.nessus_username == ""):
    
#        nessus_username = input('Nessus username ')
#        print('Nessus username entered:', nessus_username)
        
        input_str = input("Enter the nessus login username or hit enter [Default=administrator] ")
        if len(input_str) == 0:
            nessus_username = 'administrator'
        else:
            try:
                nessus_username = input_str
            except ValueError:
                print('error parsing the nessus username, aborting')
                exit()
         
    if args.nessus_password == "":
    
        try:
            nessus_password = getpass.getpass()
        except Exception as error:
            print('Error getting the nessus password, aborting', error)
            exit()
            
    

    ##TODO: nessus wizard, env vars, command line parameters, warnings if precedence is taken, warning if --nessus is not specified but username/password are
    if args.nessus_username:
        nessus_login_request_username = args.nessus_username
    else:
        nessus_login_request_username = nessus_username
        
    if args.nessus_password:
        nessus_login_request_password = args.nessus_password
    else:
        nessus_login_request_password = nessus_password


    print('\nRunning a Nessus scan...')
    print(Fore.CYAN +"requesting login session.." + Style.RESET_ALL)
    
    
    def nessus_login_request():
        login_request = requests.post (
                'https://nessus.acme.cc/session',
            json = {
                'username': nessus_login_request_username,
                "password": nessus_login_request_password   
            }
        )
        request_log(login_request)
        token = login_request.json()['token']
        return token
        
    token = nessus_login_request()


    newscan_request = requests.post (
        'https://nessus.acme.cc/scans',
        headers = {
            'X-Cookie': 'token=' + token
        },
        json = {
            'settings': {
                'attach_report': 'no',
                'description': 'Default Remote Infra',
                'emails': '',
                'enabled': False, 
                'file_targets': '',
                'filter_type': 'and', 
                'filters': [], 
                'folder_id': 3, 
                'launch_now': True,        ### start the scan
                'live_results': '', 
                'name': nessus_outputname, ##args.nessus_outputname,  ### set name ##TODO set nessus outputname
                'policy_id': '4',               ### Default Remote Infra
#                'policy_id': '329',             ### snelle testpolicy
                'scanner_id': '1',  
#                'text_targets': ', '.join(inputdomains_and_ips),
                'text_targets': nessus_text_targets

            },
        'uuid': 'ab4bacd2-05f6-425c-9d79-3ba3940ad1c24e51e1f403febe40'
        }
    )
    request_log(newscan_request)

    scan_id=newscan_request.json()['scan']['id']
    print('scan_id = ' + str(scan_id) + '\n')


    requestheaders = {
        'X-Cookie': 'token=' + token
    }

    print(Fore.CYAN +"querying scan status.." + Style.RESET_ALL)
    getscanstatus_request = requests.get ('https://nessus.acme.cc/scans/' + str(scan_id) + '?limit=2500&includeHostDetailsForHostDiscovery=true', headers = requestheaders)
    request_log(getscanstatus_request, printresponse=False)
    scanstatus = getscanstatus_request.json()['info']['status']
    
    my_date = datetime.now()
    print(my_date.isoformat("T", 'seconds'))
    
    print(Fore.MAGENTA +"Nessus scan status = " + scanstatus + Style.RESET_ALL + '\n')

    print('Do other commands first while Nessus runs..\n')

else:
    print( Fore.CYAN + 'Add ' + Fore.MAGENTA + '--nessus' + Fore.CYAN + ' switch to also do nessus scans' + Style.RESET_ALL + '\n')



# 3. Run several NMAP scans
currentuser = getpass.getuser()

if args.nmap:
    print('Running Nmap scans...')
    Path("Nmap").mkdir(parents=True, exist_ok=True)


    print("Root privileges required for nmap")
    subprocess.run(['sudo', 'echo', 'Privileges elevated for further sudo commands'], capture_output=False, text=True)
    
    nmap_subprocesses = {}
    
    for domain in domains:
        if len(domain['ipv4list_nocloudflare']) == 1: # if there is just one IPv4 address behind the domain name, we can use the domain name as input to nmap
            print(Fore.MAGENTA + 'Running nmap on: ' + Fore.BLUE + domain['url'] + Style.RESET_ALL)
            
            print("Running nmap TCP scan..")  ## result_nmap variables don't get used
            # sudo nmap -n -v -sS -A -Pn -p- -oA Nmap/test-acme-cc_65k-tcp test.acme.cc
####            result_nmap = subprocess_tee.run(['sudo', 'nmap', '-n', '-v', '-sS', '-A', '-Pn', '-p-', '-oA', 'Nmap/' + domain['url_outputname'] + '_65k-tcp', domain['url'] ], capture_output=True, text=True)
            nmap_Popen = subprocess.Popen(['sudo', 'nmap', '-p', '22-400', '-oA', 'Nmap/' + domain['url_outputname'] + '_65k-tcp', domain['url'] ], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            nmap_subprocesses[ domain['url'] + '-TCP-nmap' ] = nmap_Popen
            
            print("Running nmap UDP scan..")
            # sudo nmap -n -v -sU -A -Pn --top-ports 250 -oA Nmap/test-acme-cc_top-250-ports-udp test.acme.cc
####            result_nmap = subprocess_tee.run(['sudo', 'nmap', '-n', '-v', '-sU', '-A', '-Pn', '--top-ports', '250', '-oA', 'Nmap/' + domain['url_outputname'] + '_top-250-ports-udp',  domain['url'] ], capture_output=True, text=True)
            result_nmap = subprocess.Popen(['sudo', 'nmap', '-p', '443', '-oA', 'Nmap/' + domain['url_outputname'] + '_top-250-ports-udp',  domain['url'] ], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            nmap_subprocesses[ domain['url'] + '-UDP-nmap' ] = nmap_Popen
            
        else: # if there are multiple IPv4 addresses behind the domain name, we create a folder for the domain inside the Nmap folder, and create separate nmap output files per IPv4 address TODO also add this to command echo when --nmap flag is not specified
            Path("Nmap/" + domain['url_outputname'] ).mkdir(parents=True, exist_ok=True)
            for ipv4 in domain['ipv4list_nocloudflare']: 
            
                ipv4_outputname = ipv4.to_text().replace('.', '-')

                print(Fore.MAGENTA + 'Running nmap on: ' + Fore.BLUE + ipv4.to_text() + Style.RESET_ALL)
                print("Running nmap TCP scan..")
                # sudo nmap -n -v -sS -A -Pn -p- -oA Nmap/acme-cc/136-144-141-138_65k-tcp 136.144.141.138
####                result_nmap = subprocess_tee.run(['sudo', 'nmap', '-n', '-v', '-sS', '-A', '-Pn', '-p-', '-oA', 'Nmap/' + domain['url_outputname'] + '/' + ipv4_outputname + '_65k-tcp', ipv4.to_text()], capture_output=True, text=True)
                result_nmap = subprocess.Popen(['sudo', 'nmap', '-p', '80', '-oA', 'Nmap/' + domain['url_outputname'] + '/' + ipv4_outputname + '_65k-tcp', ipv4.to_text()], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                nmap_subprocesses[ ipv4.to_text() + '-TCP-nmap'  ] = nmap_Popen
                
                print("Running nmap UDP scan..")
                # sudo nmap -n -v -sU -A -Pn --top-ports 250 -oA Nmap/acme-cc/136-144-141-138_top-250-ports-udp 136.144.141.138
####                result_nmap = subprocess_tee.run(['sudo', 'nmap', '-n', '-v', '-sU', '-A', '-Pn', '--top-ports', '250', '-oA', 'Nmap/' + domain['url_outputname'] + '/' + ipv4_outputname + '_top-250-ports-udp', ipv4.to_text()], capture_output=True, text=True)
                result_nmap = subprocess.Popen(['sudo', 'nmap', '-p', '443', '-oA', 'Nmap/' + domain['url_outputname'] + '/' + ipv4_outputname + '_top-250-ports-udp', ipv4.to_text()], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                nmap_subprocesses[ ipv4.to_text() + '-UDP-nmap' ] = nmap_Popen
         
         
    for ip in ips_nocloudflare:
        print(Fore.MAGENTA + 'Running nmap on: ' + Fore.BLUE + ip['parsed_ip'].exploded + Style.RESET_ALL)
        print("Running nmap TCP scan..")
        result_nmap = subprocess.Popen(['sudo', 'nmap', '-n', '-v', '-sS', '-A', '-Pn', '-p-', '-oA', 'Nmap/' + ip['parsed_ip'].exploded + '-65k-tcp', ip['parsed_ip'].exploded], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        nmap_subprocesses[ ip['parsed_ip'].exploded + '-TCP-nmap'  ] = nmap_Popen
        
        print("Running nmap UDP scan..")
        result_nmap = subprocess.Popen(['sudo', 'nmap', '-n', '-v', '-sU', '-A', '-Pn', '--top-ports', '250', '-oA', 'Nmap/' + ip['parsed_ip'].exploded + '-top-250-ports-udp', ip['parsed_ip'].exploded], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        nmap_subprocesses[ ip['parsed_ip'].exploded + '-UDP-nmap'  ] = nmap_Popen
            
            
            
            
            # nmap output files are owned by root after sudo scans
            # change ownership of nmap files to regular user
            # TODO: changing the ownership of these files back to regular user pauses the scripts and if nmap took more than 5 minutes, prompts user for password, halting the script until sudo timeout
            # Commented out this section because it is very likely the default timeout of 5 minutes for sudo has been reached while nmap runs for a while:
            # solutions:
            #   - combine nmap and chown commands into one command, executed by sudo
            #   - do this at the very end of the script. prompt for other user input first so sudo password input doesn't timeout
            #   - simply keep files owned by root
            
            
    ##        tcpxmlchown   = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-65k-tcp.xml'  ], capture_output=True, text=True)
    ##        tcpnmapchown  = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-65k-tcp.nmap' ], capture_output=True, text=True)
    ##        tcpgnmapchown = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-65k-tcp.gnmap'], capture_output=True, text=True)

    ##        udpxmlchown   = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-top-250-ports-udp.xml'  ], capture_output=True, text=True)
    ##        udpnmapchown  = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-top-250-ports-udp.nmap' ], capture_output=True, text=True)
    ##        udpgnmapchown = subprocess_tee.run(['sudo', 'chown', currentuser +':'+ currentuser, 'Nmap/' + domain['url_outputname'] + '-top-250-ports-udp.gnmap'], capture_output=True, text=True)


    ## TODO
    #        for nmapfiles in ['-65k-tcp', '-top-250-ports-udp']
    #            for outputtype in ['xml', 'nmap', 'gnmap']:
    #                nmapchown_result[outputtype]= 
                
                
            

            # subprocess_tee already shows output so printing output here isn't necessary anymore
    #        if result_nmap.returncode == 0: 
    #            if args.verbose:
    #                print(result_nmap.stdout)
    #            print(Fore.GREEN + "[SUCCESS]" + Style.RESET_ALL + " Nmap ran succesfully.\n")
    #        else:
    #            print(Fore.RED + "[FAILED]" + Style.RESET_ALL + " an error has occurred running Nmap: \n" + result_nmap.stderr)
    print()
    
else:

    print(Fore.CYAN + 'Add ' + Fore.MAGENTA + '--nmap' + Fore.CYAN + ' switch to also do the following nmap scans:' + Style.RESET_ALL)
    for domain in domains:
        for ipv4 in domain['ipv4list']:
            print('Nmap command to detect open TCP ports: \n' + Fore.YELLOW + \
                'sudo nmap -n -v -sS -A -Pn -p- -oA ' + domain['url_outputname'] + '-65k-tcp ' + \
                ipv4.to_text() + Style.RESET_ALL)
            print('Nmap command to detect open UDP ports: \n' + Fore.YELLOW + \
                'sudo nmap -n -v -sU -A -Pn --top-ports 250 -oA ' + domain['url_outputname'] + \
                '-top-250-ports-udp ' + ipv4.to_text() + Style.RESET_ALL + '\n')



# 5. Run TestSSL against all ports with SSL/TLS


### 
### `testssl.sh` is called with just `testssl` on arch linux and debian/ubuntu. The ".sh" part was removed by the pacman and apt package maintainers
### solution: symlink `testssl.sh` to `testssl`:
### sudo ln -s /usr/bin/testssl /usr/bin/testssl.sh
###
### better solution: detect presence of testssl or testssl.sh binary and use the right one 
### TODO: check existence of either and use the right one so manual symlink creation isn't needed anymore

# set the right name for the testssl script command
if os.path.isfile('/usr/bin/testssl.sh'):
    print('/usr/bin/testssl.sh exists\n')
    testssl_exec = 'testssl.sh'
elif os.path.isfile('/usr/bin/testssl'):
    print('/usr/bin/testssl exists\n')
    testssl_exec = 'testssl'
else:
    print('testssl script not found\n')
    


# run testssl scan by default if the --notestssl argument is not given
if args.notestssl != True:
    print("Running TestSSL...")
    
    Path("TestSSL").mkdir(parents=True, exist_ok=True)
    
    for domain in domains:
        print(Fore.MAGENTA + 'Running testssl on: ' + Fore.BLUE + domain['url'] + Style.RESET_ALL)
        result_testssl = subprocess_tee.run([testssl_exec, '--warnings', 'batch' , domain['url']], capture_output=True, text=True) # '--log',    '--color=0',

        if result_testssl.returncode == 0:
            f = open('TestSSL/' + domain['url_outputname'] + '-testssl.txt', 'w')
            f.write(result_testssl.stdout)
            f.close()
            if args.verbose:
                print(result_testssl.stdout)
            print(Fore.GREEN + "[SUCCESS]" + Style.RESET_ALL + " TestSSL ran succesfully.")
        else:
            print(Fore.RED + "[FAILED]" + Style.RESET_ALL + " an error has occurred running TestSSL: \n" + result_testssl.stderr)












# 7 & 8. Check the HTTP headers (cURL)

## debug: for domain in domains, pp(domain), pp(domains)

result_curl = {}

def curl(curl_type):
    if (curl_type == 'head'):
        i_spec = '-I'       # -I, --head, fetch the headers only
    else:
        i_spec = '-i'       # -i, --include, include the http response headers in the output
#    print(i_spec)
    
    
    result_curl[curl_type] = subprocess_tee.run(['curl', i_spec, '-X', curl_type.upper(), domain['url'], '-L'], capture_output=True, text=True)
    # -L, --location, if the server reports that the requested page has moved to a different location, this option will make curl redo the request on the new place
    
    if result_curl[curl_type].returncode == 0: # #### = 60 bij error TODO 60 is not 0 but indicates ssl cert error
        if args.verbose:
            print(f'Method {curl_type}: \n' + result_curl[curl_type].stdout)
    else:
        print(Fore.RED + "[FAILED]" + Style.RESET_ALL + " an error has occurred running cURL: \n" + result_curl[curl_type].stderr)
        
    f = open('cURL/' + domain['url_outputname'] + F'-curl-{curl_type}.txt', 'w')
    f.write(result_curl[curl_type].stdout)
    f.close()

if args.nocurl != True:

    print("\nRunning cURL...")
    Path("cURL").mkdir(parents=True, exist_ok=True)
    
    for domain in domains:
        print(Fore.GREEN + 'cURL: ' + domain['url'] + Style.RESET_ALL)
        for curl_type in ['options', 'track', 'trace', 'head']:
            print(Fore.GREEN + 'cURL ' + curl_type.upper() + Style.RESET_ALL)
            curl(curl_type)
            
        if all([
            result_curl['options'].returncode, #### = 60 bij error TODO 60 is not 0 but indicates ssl cert error
            result_curl['track'].returncode,   #### = 60 bij error TODO
            result_curl['trace'].returncode,   #### = 60 bij error TODO
            result_curl['head'].returncode     #### = 60 bij error TODO
        ]):
            print(Fore.GREEN + "[SUCCESS]" + Style.RESET_ALL + " cURL ran succesfully.\n")  
        
else: ## provide cURL commands to run
    print('Skipping cURL scans\n')
    print('cURL commands:')
    
    for domain in domains:
        for curl_type in ['options', 'track', 'trace', 'head']:
            if (curl_type == 'head'):
                i_spec = '-I'       # -I, --head, fetch the headers only
            else:
                i_spec = '-i'       # -i, --include, include the http response headers in the output
            print('curl', i_spec, '-X', curl_type.upper(), domain['url'], '-L')
    print()
    
    
    
    
    
    
##TODO read curl head files, check if it has all the right headers:
## ['Content-Security-Policy', 'Strict-Transport-Security', 'X-Frame-Options', 'X-XSS-Protection', 'X-Content-Type-Options', 'Referrer-Policy']



# 4. Run a Nikto scan

# run nikto scan by default if the --nonikto argument is not given
if args.nonikto != True:
    print("Running Nikto...")
    Path("Nikto/Nikto_requests/").mkdir(parents=True, exist_ok=True)
    for domain in domains:

        Path("Nikto").mkdir(parents=True, exist_ok=True)
        
        
        #  +ERROR: Unable to create -Save directory 'Nikto/Nikto_requests/'
        #  have to specify absolute path to fix issue with nikto not saving requests correctly: https://github.com/sullo/nikto/issues/503#issuecomment-404882115
        pwd = str(pathlib.Path().resolve())
        
        # assumes all domains are https
        nikto_target = 'https://' + domain['url']
        
        print(Fore.GREEN + 'Running Nikto on ' + Fore.BLUE + nikto_target + Style.RESET_ALL)
        result_nikto = subprocess_tee.run(['nikto', '-h', nikto_target, '-S', pwd + '/Nikto/Nikto_requests/'], capture_output=True, text=True)
        
        #result_nikto = subprocess_tee.run(['nikto', '-h', domain['url'] ], capture_output=True, text=True)
        

        if result_nikto.returncode == 0:
	        f = open('Nikto/' + domain['url_outputname'] + '-nikto.txt', 'w')
	        f.write(result_nikto.stdout)
	        f.close()
	        if args.verbose:
		        print(result_nikto.stdout)
	        print(Fore.GREEN + "[SUCCESS]" + Style.RESET_ALL + " Nikto ran succesfully.\n")
        else:
	        print(Fore.RED + "[FAILED]" + Style.RESET_ALL + " an error has occurred running Nikto: \n" + result_nikto.stderr)

# else: TODO: provide command if nikto is disabled
#print('\nNikto command to run: \n' + Fore.YELLOW + 'nikto -h ' + domain['url_outputname'] + \
#	  ' -output \"nikto.txt\" -S \"Nikto_requests/\"' + Style.RESET_ALL)




#### slowhttptest disabled as part of script and only the command is given to run manually
## 9. Perform a Slow HTTP test  ! Only on TEST environment.
##if args.slowhttptest:
##	print("Running SlowHttpTest...")
##	Path("SlowHTTPTest").mkdir(parents=True, exist_ok=True)
##	result_slowhttp = subprocess.run(['slowhttptest', '-c', '1000', '-B', '-g', '-o', 'SlowHTTPTest/' + domain['url'] + \
##	'-slowhttptest', '-i', '110', '-r', '200', '-s', '8192', '-t', 'FAKEVERB', '-u', domain['url'], '-x', '10', '-p', '3'], \
##	capture_output=True, text=True)

##	if result_slowhttp.returncode == 0:
##		if args.verbose:
##			print(result_slowhttp.stdout)
##		print(Fore.GREEN + "[SUCCESS]" + Style.RESET_ALL + " SlowHttpTest ran successfully.")
##	else:
##		print(Fore.RED + "[FAILED]" + Style.RESET_ALL + " an error has occurred running SlowHttpTest: \n" + result_slowhttp.stderr)
##else:
##    print(Fore.CYAN + 'Add ' + Fore.MAGENTA + '--slowhttptest' + Fore.CYAN + ' switch to also do SlowHTTPTest scans.' + Style.RESET_ALL)
print(Fore.RED +  '!! perform SlowHTTPTest ONLY ON TEST ENVIRONMENT !!' + Style.RESET_ALL)
for domain in domains:
    print('Command to run SlowHTTPTest: \n' + Fore.YELLOW + 'slowhttptest -c 1000 -B -g -o ' + \
    domain['url_outputname'] +'-slowhttptest -i 110 -r 200 -s 8192 -t FAKEVERB -u ' + domain['url'] +' -x 10 -p 3' + Style.RESET_ALL + '\n')



if args.optional: ## TODO fix section
	# 11. Run lazyrecon on the host
	print('\n\n[OPTIONAL] Command to run LazyRecon: \n' + Fore.YELLOW + \
		  './lazyrecon.sh -d ' + hostname + ' -s' + Style.RESET_ALL)

	# 12. Run Netcraft to check domain info
	print('\n[OPTIONAL] Run Netcraft to get additional domain info: ' + Fore.BLUE + \
		  'https://sitereport.netcraft.com/' + Style.RESET_ALL)

	# 13. If you come across a VPN try ike-scan
	# Command has to be checked
	print('\n[OPTIONAL] Run Ike-Scan if you come across a VPN: \n' + Fore.YELLOW + \
		  'ike-scan -A -M -P ike-scan-' + domain['ipv4'] + ' -d <port> ' + domain['ipv4'] + Style.RESET_ALL)

	# 14. If NMAP or Nessus shows ports are open try to make a connection (see FTP/Telnet/NetCat)
	print('\n[OPTIONAL] Connect to ports using FTP or Telnet: \n' + Fore.YELLOW + \
		  'ftp ' + domain['ipv4'] + '\ntelnet ' + domain['ipv4'] + Style.RESET_ALL)

	# 15. If SSH port is open use the tool SSHScan
	# Contains pseudo command, needs to be fixed
	print('\n[OPTIONAL] Run SSHScan if SSH port is open: \n' + Fore.YELLOW + \
		  'python /location/of/sshscan/sshscan.py -t ' + ipv4 + Style.RESET_ALL)  # TODO 

	# 16. In case of IIS webservers => run the IIS-ShortName-Scanner
	print('\n[OPTIONAL] Run IIS-ShortName-Scanner if the webserver is IIS: \n' + Fore.YELLOW + \
		  'java -jar iis_shortname_scanner.jar 2 20 ' + domain['url_outputname'] + Style.RESET_ALL)						


# define nessus helper functions
def download_all_scan_results():
    download_xml_report()
    download_html_export()
    


def download_xml_report():
    print(Fore.CYAN +"requesting xml file download.." + Style.RESET_ALL)
    scanreportxmldownloadtoken_post_request = requests.post( ## request download xml file
        'https://nessus.acme.cc/scans/' + str(scan_id) + '/export?limit=2500', 
        headers = {'X-Cookie': 'token=' + token},
        json = {"format":"nessus"}
    )
    request_log(scanreportxmldownloadtoken_post_request)
    
    
    time.sleep(1) ## give request above time to finish before trying to assign a variable with its response, for more verbose error handling
    downloadtoken = scanreportxmldownloadtoken_post_request.json()['token']
    #print(downloadtoken)

    
    
    print(Fore.CYAN +"downloading xml file" + Style.RESET_ALL)
    scanreportxmldownloadtoken_get_request = requests.get(
        'https://nessus.acme.cc/tokens/' + downloadtoken + '/download' 
    )

    request_log(scanreportxmldownloadtoken_get_request, printresponse=False)

    if args.nessus_outputname:
        filename = args.nessus_outputname + "-nessus_export.xml"
    else:
        filename =                          "nessus_export.xml" ##TODO
        
    with open("Nessus/" + filename, "w") as file: 
        file.write(scanreportxmldownloadtoken_get_request.text)
    print(Fore.GREEN +"file saved as " + Fore.WHITE + filename + Style.RESET_ALL)
    print()



def download_html_export():
    scanreportdownloadtoken_post_request = [""] * 2
    htmldownloadtokens = [""] * 2
    scanreporthtmldownloadtoken_post_requests = [""] * 2
        
        
    # for sort_vuln_by in ['host', 'plugin']: ## download two html report files: one with vulnerabilities sorted by host, and the other one sorted by plugins
    for i, sort_vuln_by in enumerate(["host", "plugin"]): ## download two html report files: one with vulnerabilities sorted by host, and the other one sorted by plugins
    
        # scanreporthtmldownloadtoken_post_requests["scanreport_sorted_by_{0}_post_request".format(sort_vuln_by)] = requests.post( ## request download 1st html file, sorted by host
        print(Fore.CYAN +"requesting html file sorted-by-" + sort_vuln_by + " download.." + Style.RESET_ALL)
        scanreporthtmldownloadtoken_post_requests[i] = requests.post( ## request download sorted by host; sorted by plugin
            'https://nessus.acme.cc/scans/' + str(scan_id) + '/export?limit=2500', 
            headers = {'X-Cookie': 'token=' + token},
            json = {
                "chapters": "custom;vuln_by_" + sort_vuln_by + ";vulnerabilities",
                "extraFilters": {
                    "host_ids": [],
                    "plugin_ids": []
                },
                "format": "html",
                "reportContents": {
                    "csvColumns": {},
                    "formattingOptions": {
                        "page_breaks": "true"
                    },
                    "hostSections": {
                        "host_information": "true",
                        "scan_information": "true"
                    },
                    "vulnerabilitySections": {
                        "cvss_base_score": "true",
                        "cvss_temporal_score": "true",
                        "cvss3_base_score": "true",
                        "cvss3_temporal_score": "true",
                        "description": "true",
                        "exploitable_with": "true",
                        "plugin_information": "true",
                        "plugin_output": "true",
                        "references": "true",
                        "risk_factor": "true",
                        "see_also": "true",
                        "solution": "true",
                        "stig_severity": "true",
                        "synopsis": "true"
                    }
                }
            }
        )
        request_log(scanreporthtmldownloadtoken_post_requests[i])
        htmldownloadtokens[i] = scanreporthtmldownloadtoken_post_requests[i].json()['token']
        # print('htmldownloadtoken = ' + htmldownloadtokens[i])

        time.sleep(7)

        print(Fore.CYAN +"downloading html file sorted-by-" + sort_vuln_by + " file" + Style.RESET_ALL)
        scanreporthtmldownloadtoken_post_requests[i] = requests.get(
            'https://nessus.acme.cc/tokens/' + htmldownloadtokens[i] + '/download' 
            # headers = {'X-Cookie': 'token=' + token},
        )
        request_log(scanreporthtmldownloadtoken_post_requests[i], printresponse=False)
        
        if args.nessus_outputname:
            filename = args.nessus_outputname + "-nessus_report_sort-by-" + sort_vuln_by + ".html" 
        else:
            filename =                          "nessus_report_sort-by-" + sort_vuln_by + ".html" 
        
        
        with open("Nessus/" + filename, "w") as file:  
            file.write(scanreporthtmldownloadtoken_post_requests[i].text)
        print(Fore.GREEN +"file saved as " + Fore.WHITE + filename + Style.RESET_ALL)
        print()



def delete_nessus_scan(): ## delete nessus scan from nessus host after downloading reports, to keep it clean.


    # move scan to trash first, not required with API, only in interface
##    move_scan_to_trash_request = requests.put(
##        'https://nessus.acme.cc/scans/' + scan_id + '/folder',
##        headers = {
##            'X-Cookie': 'token=' + token,
##            'X-API-Token': 'CCA04DCB-6B2D-4F77-9D06-6DB8A121A88B'
##        },
##        json = {
##            'folder_id': "2"  ##  folder_id 2 = trash  
##        }
##    ) 
##    request_log(move_scan_to_trash_request)
    


    # delete scan
    delete_nessus_scan_request = requests.delete(
        'https://nessus.acme.cc/scans',
        headers = {
            'X-Cookie': 'token=' + token,
            'X-API-Token': 'CCA04DCB-6B2D-4F77-9D06-6DB8A121A88B'
        },
        json = {
	        "ids": [
		        scan_id
	        ]
        }
    ) 
    request_log(delete_nessus_scan_request)



if args.nessus:
    
    ## renew login token in case the previous one expired in the meantime
    token = nessus_login_request()
    
    checkstatus_interval = 10  ## seconds in between each successive scan status check
    print('waiting for nessus scan to complete. Checking every ' + str(checkstatus_interval) + ' seconds')


######################
#    def is_scan_completed(request):
#        status = request.json()['info']['status']
#        my_date = datetime.now()
#        print(my_date.isoformat("T", 'seconds'))
#        print(Fore.LIGHTBLUE_EX + "nessus scan id " + str(scan_id) + " status = " + status + Style.RESET_ALL) # TODO timestamp, scan name/hostname
#        return status == 'completed'
#    #    return status == 'canceled'    


#    polling2.poll(
#        lambda: requests.get('https://nessus.acme.cc/scans/' + str(scan_id) + '?limit=2500&includeHostDetailsForHostDiscovery=true', headers = requestheaders),
#            check_success=is_scan_completed,
#            step = checkstatus_interval,
#            poll_forever=True # keep trying indefinitely until the
#            #print('test'),
#        #    poll_forever=True,
#        #    log=logging.INFO
#    )
######################
#    polling.poll(
#    lambda: requests.get('https://nessus.acme.cc/scans/' + str(scan_id) + '?limit=2500&includeHostDetailsForHostDiscovery=true', headers = requestheaders).json()['info']['status'] == 'completed,
#    step=10,
#    poll_forever=True)
###############
    
#       
    
    nessus_status = ""
    while nessus_status != 'completed':
        nessus_status = requests.get('https://nessus.acme.cc/scans/' + str(scan_id) + '?limit=2500&includeHostDetailsForHostDiscovery=true', headers = requestheaders).json()['info']['status']
        my_date = datetime.now()
        print(my_date.isoformat("T", 'seconds'))
        print(Fore.LIGHTBLUE_EX + "nessus scan id " + str(scan_id) + " status = " + nessus_status + Style.RESET_ALL) # TODO timestamp, scan name/hostname
        time.sleep(checkstatus_interval)  # wait nr of seconds before doing the request again

    print('Nessus scan is done, downloading Nessus report files')
    download_all_scan_results()
    #delete_nessus_scan() ##TODO interactive question to delete nessus scan [y/N], give nessus link to scan results
    
    


# all subprocesses should be finished
#>>> all([True, True])
#True
#>>> all([True, True, False])
#False



finished = False

while finished == False:
    subprocess_returncodes = []
    for subprocess in nmap_subprocesses.values():
        subprocess_returncodes.append(subprocess.poll())
    finished = all(returncode == 0 for returncode in subprocess_returncodes)
    print(F'{finished = }')

    time.sleep(1)






# test_nmap_Popen
# TODO only if nmap ran can you check the subprocesses
if args.nmap:
    pass


##for nmap_subprocess in nmap_subprocesses:
#for i in range(0, 3):
#    poll = nmap_Popen.poll()
#    if poll is None:
#        print('process is alive')
#    else:
#        print('process has finished')
#    time.sleep(1)


## if process is not finished when it reaches this point, the script will wait for this to complete before it continues
#stdout, stderr = nmap_Popen.communicate()

print()
print(' '.join(sys.argv))
print(Fore.GREEN + 'Done' + Style.RESET_ALL)




# colorama
deinit()



